import { fetchChat } from '../actions/core.actions';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { getChats } from '../reducers/chat.reducer';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.less'],
})
export class ChatComponent implements OnInit {
  chats$ = this.store.select(getChats)
  constructor(private store: Store) {}

  ngOnInit(): void {
    this.store.dispatch(fetchChat())
  }
}

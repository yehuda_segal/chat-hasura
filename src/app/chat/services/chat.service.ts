import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class ChatService {
  user = 1;

  constructor(private http: HttpClient) {}

  fetchChats() {
    console.log(432);
    return this.http
      .post('http://localhost:8080/v1/graphql/', {
        query: ` query MyQuery {
        chats {
          id
          user_one
          user_two
          messages
        }
      }`,
      })
      .pipe(
        map((f: any) => {
          return f.data.chats;
        })
      );
  }
}

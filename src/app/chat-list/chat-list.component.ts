import { setCurrentChat } from './../actions/core.actions';
import { getCurrentChat } from './../reducers/chat.reducer';
import { Store } from '@ngrx/store';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'chat-list',
  templateUrl: './chat-list.component.html',
  styleUrls: ['./chat-list.component.less'],
})
export class ChatListComponent {
  @Input() chats: any;

  chooseChat(chat: any) {
    this.store.dispatch(setCurrentChat({ chat }));
  }

  constructor(private store: Store) {}
}

import { createAction, props } from '@ngrx/store';
import { User } from 'src/app/reducers/user.reducer';

export interface LoginUser {
  userName: string;
  password: string;
}

export interface RegisterUser extends LoginUser {
  firstName: string;
  lastName: string;
}

enum LoginActionType {
  LOGIN = '[ Login ] Login',
  LOGIN_SUCCESS = '[ Login ] Login Success',
  USER_LOGIN_FAILED = '[ Login ] Login Failed',
  REGISTER = '[ Login ] Register',
  REGISTER_SUCCESS = '[ Login ] Register Success',
}

export const registerUserSuccess = createAction(
  LoginActionType.REGISTER_SUCCESS,
  props<User>()
);

export const loginUser = createAction(
  LoginActionType.LOGIN,
  props<LoginUser>()
);

export const registerUser = createAction(
  LoginActionType.REGISTER,
  props<{ user: RegisterUser }>()
);

export const userLoginSuccess = createAction(
  LoginActionType.LOGIN_SUCCESS,
  props<User>()
);

export const userLoginFailed = createAction(
  LoginActionType.USER_LOGIN_FAILED,
  props<{ message: string }>()
);

import { LoginEffects } from './effects/login.effects';
import { NgModule } from '@angular/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { EffectsModule } from '@ngrx/effects';

@NgModule({
  imports: [EffectsModule.forFeature([LoginEffects])],
  declarations: [],
  exports: [],
})
export class LoginModule {}

import { getLoginError } from './../reducers/login.reducer';
import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { loginUser, registerUser } from '../actions/login.actions';

@Component({
  templateUrl: './login.component.html',
  selector: 'login',
  styleUrls: ['./login.component.less'],
})
export class LoginComponent {
  loginMode = false;
  loginError$ = this.store.select(getLoginError);
  userName: string;
  password: string;
  lastName: string;
  firstName: string;

  add() {
    this.store.dispatch(
      registerUser({
        user: {
          userName: this.userName,
          firstName: this.firstName,
          lastName: this.lastName,
          password: this.password,
        },
      })
    );
  }

  toggleLoginRegister() {
    this.loginMode = !this.loginMode;
  }

  login() {
    this.store.dispatch(
      loginUser({ userName: this.userName, password: this.password })
    );
  }

  constructor(private store: Store) {}
}

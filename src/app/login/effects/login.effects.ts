import { HasuraService } from './../../services/hasura.service';
import { map, switchMap } from 'rxjs/operators';
import {
  loginUser,
  RegisterUser,
  registerUser,
  registerUserSuccess,
  userLoginFailed,
  userLoginSuccess,
} from './../actions/login.actions';
import { createEffect, Actions, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { gql } from 'apollo-angular';
import { User } from 'src/app/reducers/user.reducer';
import { Router } from '@angular/router';

@Injectable()
export class LoginEffects {
  login$ = createEffect(() =>
    this.actions$.pipe(
      ofType(loginUser),
      switchMap((user) => {
        const queryName = 'login';
        const query = gql`query ${queryName} {
            ${queryName}: users(where: {userName: {_eq: "${user.userName}"}, password: {_eq: "${user.password}"}}) {
              firstName
              lastName
              userName
              lastSeen
            }
          }`;
        return this.hasuraService.query<User>(query, queryName, null);
      }),
      map((data) => {
        console.log(data);
        return data[0]
          ? userLoginSuccess(data[0])
          : userLoginFailed({ message: 'אין משתמש כזה' });
      })
    )
  );

  userLoginSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(userLoginSuccess),
        map(() => this.router.navigate(['chat']))
      ),
    { dispatch: false }
  );

  registerUser$ = createEffect(() =>
    this.actions$.pipe(
      ofType(registerUser),
      switchMap(({ user }) => {
        const queryName = 'register';
        const query = gql`mutation ${queryName}($objects: [users_insert_input!]!) {
          ${queryName}: insert_users(objects: $objects, on_conflict: {constraint: users_pkey}) {
            returning {
              firstName
              lastName
              userName
            }
          }
        }
        `;

        return this.hasuraService.upsert<User, RegisterUser>(query, queryName, {
          objects: [user],
        });
      }),
      map((data) => userLoginSuccess(data.returning[0]))
    )
  );

  constructor(
    private actions$: Actions,
    private hasuraService: HasuraService,
    private router: Router
  ) {}
}

import { userLoginFailed } from './../actions/login.actions';
import {
  createFeatureSelector,
  createReducer,
  createSelector,
  on,
} from '@ngrx/store';

export interface LoginState {
  loginError?: string;
}

const initialLoginState: LoginState = {
  loginError: null,
};

export const loginReducerToken = 'login reducer';

export const LoginReducer = createReducer(
  initialLoginState,
  on(userLoginFailed, (state, { message }) => ({
    ...state,
    loginError: message,
  }))
);

export const getLoginState =
  createFeatureSelector<LoginState>(loginReducerToken);

export const getLoginError = createSelector(
  getLoginState,
  (state) => state.loginError
);

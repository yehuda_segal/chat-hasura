import { fetchChat, fetchChatsSuccess } from '../actions/core.actions';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { map, switchMap } from 'rxjs/operators';
import { ChatService } from '../chat/services/chat.service';

@Injectable()
export class ChatEffects {
  constructor(private actions$: Actions, private chatService: ChatService) {}

  fetchChat$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(fetchChat),
        switchMap(() => this.chatService.fetchChats()),
        map((chats) => fetchChatsSuccess({ chats }))
      ),
  );
}

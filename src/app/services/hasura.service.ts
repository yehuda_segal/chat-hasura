import { map, Observable } from 'rxjs';
import { Apollo } from 'apollo-angular';
import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class HasuraService {
  constructor(private apollo: Apollo) {}
  query<T, U = T>(
    request,
    queryName: string,
    vars: { objects: U[] }
  ): Observable<T[]> {
    return this.apollo
      .query({ query: request, variables: vars })
      .pipe(map((data) => data.data[queryName]));
  }

  upsert<T, U = T>(
    request,
    queryName: string,
    vars: { objects: U[] }
  ): Observable<{returning: T[]}> {
    return this.apollo
      .mutate({ mutation: request, variables: vars })
      .pipe(map((data) => data.data[queryName]));
  }
}

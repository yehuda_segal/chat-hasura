import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { map } from 'rxjs/operators';
import { getCurrentChat } from '../reducers/chat.reducer';

@Component({
  selector: 'current-chat',
  templateUrl: './current-chat.component.html',
  styleUrls: ['./current-chat.component.less'],
})
export class CurrentChatComponent {
  currentChat$ = this.store.select(getCurrentChat).pipe(
    map((chat) => {
      
      return chat;
    })
  );

  constructor(private store: Store ) {}
}

import { setCurrentChat } from './../actions/core.actions';
import { fetchChatsSuccess } from '../actions/core.actions';
import {
  createFeatureSelector,
  createReducer,
  createSelector,
  on,
} from '@ngrx/store';

export const chatReducerToken = 'chat-token';

export interface ChatState {
  chats: any;
  currentChat: any;
}

const initialChatState: ChatState = {
  chats: [],
  currentChat: null,
};

export const chatReducer = createReducer(
  initialChatState,
  on(fetchChatsSuccess, (state, { chats }) => ({
    ...state,
    chats,
  })),
  on(setCurrentChat, (state, { chat }) => {
    console.log(chat);

    return {
      ...state,
      currentChat: chat,
    };
  })
);

export const getChatState = createFeatureSelector<ChatState>(chatReducerToken);

export const getChats = createSelector(getChatState, (state) => state.chats);

export const getCurrentChat = createSelector(
  getChatState,
  (state) => state.currentChat
);

import { userLoginSuccess } from './../login/actions/login.actions';
import { createReducer, on } from '@ngrx/store';

export interface User {
  firstName: string;
  lastName: string;
  userName: string;
  lastSeen?: Date;
}

export const initialUserState: User = {
  firstName: '',
  lastName: '',
  lastSeen: null,
  userName: '',
};

export const userReducerToken = 'UserReducer';

export const userReducer = createReducer(
  initialUserState,
  on(userLoginSuccess, (state, { firstName, lastName, userName }) => ({
    ...state,
    firstName,
    lastName,
    userName,
  })),
);

import { loginReducerToken, LoginReducer } from './login/reducers/login.reducer';
import { userReducer, userReducerToken } from './reducers/user.reducer';
import { LoginModule } from './login/login.module';
import { LoginComponent } from './login/component/login.component';
import { ChatService } from './chat/services/chat.service';
import { ChatEffects } from './effects/chat.effects';
import { chatReducer, chatReducerToken } from './reducers/chat.reducer';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChatComponent } from './chat/chat.component';
import { ChatListComponent } from './chat-list/chat-list.component';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { CurrentChatComponent } from './current-chat/current-chat.component';
import { MaterialModule } from './material.module';
import { HttpLink } from 'apollo-angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { ApolloModule, APOLLO_OPTIONS } from 'apollo-angular';
import { InMemoryCache } from '@apollo/client/core';

@NgModule({
  providers: [
    {
      provide: APOLLO_OPTIONS,
      useFactory: (httpLink: HttpLink) => {
        return {
          cache: new InMemoryCache(),
          link: httpLink.create({
            uri: 'http://localhost:8080/v1/graphql',
          }),
        };
      },
      deps: [HttpLink],
    },
    ChatService,
  ],
  declarations: [
    AppComponent,
    ChatComponent,
    LoginComponent,
    ChatListComponent,
    CurrentChatComponent,
  ],
  imports: [
    StoreModule.forRoot([]),
    StoreModule.forFeature(chatReducerToken, chatReducer),
    StoreModule.forFeature(userReducerToken, userReducer),
    StoreModule.forFeature(loginReducerToken, LoginReducer),
    EffectsModule.forRoot([ChatEffects]),
    BrowserModule,
    AppRoutingModule,
    LoginModule,
    HttpClientModule,
    ApolloModule,
    MaterialModule,
    BrowserAnimationsModule,
    FormsModule,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}

import { createAction, props } from '@ngrx/store';

enum ChatActions {
  FETCH_CHAT = '[ Chat ] Fetch Chat',
  FETCH_CHAT_SUCCESS = '[ Chat ] Fetch Chat Success',
  SET_CURRENT_CHAT = '[ Chat ] Set Current Chat',
  FETCH_USERS = '[ User ] Fetch Users',
  FETCH_USERS_SUCCESS = '[ User ] Fetch Users Success'
}

export const fetchChat = createAction(ChatActions.FETCH_CHAT);
export const fetchUsers = createAction(ChatActions.FETCH_USERS);
export const fetchUsersSuccess = createAction(ChatActions.FETCH_CHAT_SUCCESS, props<{users: any}>())
export const fetchChatsSuccess = createAction(
  ChatActions.FETCH_CHAT_SUCCESS,
  props<{ chats: any }>()
);
export const setCurrentChat = createAction(
  ChatActions.SET_CURRENT_CHAT,
  props<{ chat: any }>()
);
